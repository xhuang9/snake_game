/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function snake_object(){
    this.x = 1;
    this.y = 1;
    this.xspeed = 1;
    this.yspeed = 0;
    this.tails = 0;
    this.direction ={};
    
    this.dir = function(x,y){
        this.xspeed = x;
        this.yspeed = y;
        this.direction = {x:x, y:y};
    }
    
    this.update = function(){
        this.x = this.x + this.xspeed*scale;
        this.y = this.y + this.yspeed*scale;
    }
    
    this.clean = function(x,y){
        snake.beginPath();
        snake.rect(x, y, scale, scale);
        snake.fillStyle = "black";
        snake.fill();
    }
    
    this.show = function(){
        snake.beginPath();
        if(this.x > (frame - scale)){
            this.x = frame - scale;
            gameover();
            console.log("die");
        }
        if(this.y > (frame - scale)){
            this.y = frame - scale;
            gameover();
            console.log("die");
        }
        if(this.x < 1){
            this.x = 1;
            gameover();
            console.log("die");
        }
        if(this.y < 1){
            this.y = 1;
            gameover();
            console.log("die");
        }
        snake.rect(this.x, this.y, scale, scale);
        snake.fillStyle = "green";
        snake.fill();
    }
    
    this.showTails = function(object){
        snake.beginPath();
        snake.rect(object.x, object.y, scale, scale);
        snake.fillStyle = "green";
        snake.fill();
    }
    
    this.addtail = function(){
        this.tails = this.tails + 1;
        //somehow record the direction
    }
    
    this.getPosition = function(){
        var position = {x:this.x, y:this.y};
        return position;
    }
    
    this.getDirection = function(){
        return this.direction;
    }
        
    this.getTails = function(){
        return this.tails;
    }
}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//canvas settings
var canvas = document.getElementById("myCanvas");
var snake = canvas.getContext("2d");

var scale = 20;//how big the snake is
var s = new snake_object;//snake object
var f = new food;
var timeout = 800;//set the speed of the game
var frame = 601;//set how big the canvas frame for the game
var tailInfo = [];
var img = new Image();
img.src = "picture/gameover.png";

function setSpeed1(){
    timeout = 100;
    return timeout;
}
function setSpeed2(){
    timeout = 200;
    return timeout;
}
function setSpeed3(){
    timeout = 500;
    return timeout;
}

function gameover(){
   alert("game over!");
   timeout = 200000;
   return timeout;
}

//the following code is the looping part
var action = function(x, y) {
    //this part is to set new food location when the game is initiated.
    if(typeof(x)==='undefined'){
        x = 1;
        f.setNewfoodlocation();
    }
    if(typeof(y)==='undefined'){
        y = 1;
    }
    snake.clearRect(-1,-1,602,602);
    TailLong = s.getTails();
    var tail = [];
//    console.log("taillong:");
//    console.log(TailLong);
    for(i = 1; i <= TailLong; i++){
//        console.log(i);
//        console.log(tailInfo[tailInfo.length-i]);
        s.showTails(tailInfo[tailInfo.length-i]);
        tail.push(tailInfo[tailInfo.length-i]);
    }
    s.show();//show the snake head
    f.show();//show the food
    p = s.getPosition();//get previouslocation of snake
    console.log(p);
    x= p.x;
    y= p.y;
    for(i=0;i <tail.length; i++){
        if(tail[i].x === p.x && tail[i].y === p.y){
            gameover();
            console.log("die");
        }
    }
    eaten = f.eat(x, y);
    if(eaten){
      s.addtail();  
    }
    tailInfo.push(s.getPosition());
    s.update();
    setTimeout(action, timeout, x, y);
};
action();

//the following code is key press function
$(document).bind('keyup', function(event){
    switch(event.which){
        case 32://blank_space for pause the game
            s.dir(0, 0);
            break;
        case 37://left
            if(s.getDirection().x ==1 && s.getDirection().y ==0){//snake can not go reverse
            }else{
                s.dir(-1,0);
            }
            break;
        case 38://up
            if(s.getDirection().x ==0 && s.getDirection().y ==1){
            }else{
                s.dir(0,-1);
            }
            break;
        case 39://right
            if(s.getDirection().x ==-1 && s.getDirection().y ==0){
            }else{
                s.dir(1,0);
            }
            break;
        case 40://down
            if(s.getDirection().x ==0 && s.getDirection().y ==-1){
            }else{
                s.dir(0,1);
            }
            break;
    }
});

